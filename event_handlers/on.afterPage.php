<?
//this function is called after any page is constructed
function on_afterPage()
{
	//output to browser the execution times
	global $timer;
	$timer->Stop("WebApp");
	$timer->OutputInfo();
}
?>