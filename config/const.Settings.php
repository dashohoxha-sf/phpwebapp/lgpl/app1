<?
//The constants defined here change the behaviour of the framework
//and the application. You can change the values of the constants
//according to the instructions given in comments, or add new
//constants that you use in your application

define("USES_DB", false);
	//if this constant is true, the framework will load the DB component
	//and will open a default connection with the db specified in the
	//file 'config/const.DB.php'

define("DEBUG_GOTO", false);
	//if this constant is true, the framework displays an alert
	//each time that the function GoTo() is called	(for debug)

define("DEBUG_SESSION", false);
	//if this constant is true, the framework outputs the session
	//variables as an HTML table (for debug)

define("DEBUG_RECORDSETS", false);
	//if this constant is true, the framework outputs all the 
	//recordsets of the page and their contents (for debug)

define("DEBUG_TEMPLATES", false);
	//if this constant is true, the framework outputs the tree
	//structure of the templates of the page (for debug)

define("EXECUTION_TIME_INFO", false);
	//if this constant is true, the framework outputs information
	//about the execution time of several processes (for debug)

//etc.
?>
